// Embedded Data Examples

let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/blog_demo', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

// POST - title, content
// must be defined before User so that postSchema is defined to be used in the userSchema
let postSchema = new mongoose.Schema({
  title: String,
  content: String,
});
let Post = mongoose.model('Post', postSchema);

// USER - email, name
let userSchema = new mongoose.Schema({
  email: String,
  name: String,
  posts: [postSchema],   // telling mongoose we want it to be a list of posts
});
let User = mongoose.model('User', userSchema);

// a User will have this format:
// {
//   email:'asdas',
//   name: 'asdas',
//   posts: [
//     {title: 'asdas', content: 'asdas'},
//     {title: 'asdas', content: 'asdas'},
//     {title: 'asdas', content: 'asdas'}
//   ]
// }

// Make User
// let newUser = new User({
//   email: 'hermione@hogwarts.edu',
//   name: 'Hermione Granger',
// });
// newUser.posts.push({
//   title: 'How to brew Polyjuice Potion',
//   content: 'Just kidding. Go to potions class to learn it!'
// });
// newUser.save((err, user) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(user);
//   }
// });

// Find User
User.findOne({name: 'Hermione Granger'}, (err, user) => {
  if (err) {
    // console.log(err);
  } else {
    user.posts.push({
      title: 'Three Things I Really Hate',
      content: 'Voldemort. Voldemort. Voldemort.',
    });
    user.save((err, user) => {  // some people would call this callback hell :)
      if (err) {
        console.log(err);
      } else {
        console.log(user);
      }
    });
  }
});
