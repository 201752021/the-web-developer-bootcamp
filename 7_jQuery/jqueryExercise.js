// below will test both that jQuery is loaded and .js file is connected

// if(jQuery) {
//   alert('jQuery Loaded!');
// } else {
//   alert('No jQuery :(');
// }

// select all divs and give them a purple background
$('div').css('background', 'purple');

// select divs with class 'highlight' and make them 200px wide
$('.highlight').css('width', '200px');

// select the div with id 'third' and give it an orange border
$('#third').css('border', 'orange solid 4px');    // picked solid & 4px b/c weren't specified

// Bonus: select the first div only and change its font color to pink
    // my attempts
    // $('div')[0].css('color', 'pink');          // DOESN'T WORK
    // $('div').first().css('color', 'pink');     // worked when changed 'font-color' to 'color'
    // $('div').get(0).css('color', 'pink');      // DOESN'T WORK
$('div:first-of-type').css('color', 'pink');
// or
$('div').first().css('color', 'pink');  // actually slower than 'first-of-type' because it is a
                                                  // jQuery shortcut rather than an option built into css
