// $('button').on('click', function() {
//   $('div').fadeOut(1000, function() {     // not deleting, just hiding (display: none)
//     $('div').remove();                    // this anon function makes javascript wait until fade is
//   });                                     // completed it removed the elements --- if it didn't wait
// });                                       // the order wouldn't be guaranteed and it might
//                                           // delete them before the fade completes

// $('button').on('click', function() {
//   $('div').fadeToggle(500);
// });

// $('button').on('click', function() {
//   $('div').slideDown();
// });

// $('button').on('click', function() {
//   $('div').slideUp();
// });

$('button').on('click', function() {
  $('div').slideToggle();
});
