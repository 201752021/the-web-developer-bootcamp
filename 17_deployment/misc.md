# things to add to package.json for gitlab ci:

"engines": {
  "node": "13.13.0",
  "npm": "6.14.8"
},
"repository": {
  "type": "git",
  "url": "git+ssh://git@gitlab.com/kristinbrooks/heroku-deployment-demo-kristin.git"
},
  
"bugs": {
"url": "https://gitlab.com/kristinbrooks/heroku-deployment-demo-kristin/issues"
},

"homepage": "https://gitlab.com/kristinbrooks/heroku-deployment-demo-kristin#readme"


# demo app site:

https://aqueous-everglades-32558.herokuapp.com/ | https://git.heroku.com/aqueous-everglades-32558.git
  