// making a weather API request

const axios = require('axios');

(async () => {
  try {
    const response = await axios.get('http://www.7timer.info/bin/api.pl?lon=113.17&lat=23.09&product=astro&output=json');
    console.log(response.data.dataseries[0].prec_type);
  } catch(err) {
    console.log(err);
  }
})();
