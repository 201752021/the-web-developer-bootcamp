// Making an API request (video uses deprecated request package so axios is being used instead)

const axios = require('axios');

(async () => {
  try {
    const response = await axios.get('https://www.google.com');
    console.log(response.data);   // gets google.com's html
  } catch(err) {
    console.log(err);
  }
})();
