// Making requests from an API using axios

const axios = require('axios');

// THENABLE PROMISE VERSION:

// this is called method chaining: .get->.then->.catch->.finally
// if there is a .then, then .get returns a promise that we can catch an error for
axios.get('https://jsonplaceholder.typicode.com/todos/1')
// using `/1` so we only get back one response and not hundreds
  .then(function (response) {
    // handle success
    console.log(response.data);         // can specify which specific data we want so we don't
    console.log(response.data.title);   // get everything back
    console.log(response.data.userId);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });


// ASYNC/AWAIT VERSION:

// the following code gives an error that says 'await is only valid in async function'
  // try {
  //   const response = await axios.get('https://jsonplaceholder.typicode.com/todos/1');
  // } catch(err) {
  //   console.log(err);
  // }

// would normally be running the code within a route using express like below --- it is now in an
// async function
  // app.get('/', async (req, res, next) => {  // can replace the word 'function' with an arrow function
  //   try {
  //     const response = await axios.get('https://jsonplaceholder.typicode.com/todos/1');
  //     console.log(response.data);
  //   } catch(err) {
  //     console.log(err);
  //   }
  // });

// async not using express
(async () => {
  try {
    const response = await axios.get('https://jsonplaceholder.typicode.com/todos/1');
    console.log(response.data);
  } catch(err) {
    console.log(err);
  }
})();
