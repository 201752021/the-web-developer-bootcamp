// This is a very basic app to introduce writing routes.

let express = require('express');
let app = express();

// ROUTES
// '/' => 'Hi There!'
app.get('/', function (req, res) {   // params: (url, callback function(request, response){})
  // req and res are objects
  // req contains all the info about the request that was made, that triggered this route
  // res contains all the information we're going to respond with
  res.send('Hi There!');
});

// '/bye' => 'Goodbye!'
app.get('/bye', function(req, res) {
  res.send('Goodbye!');
});

// '/dog' => 'MEOW!'
app.get('/dog', function(req, res) {
  res.send('MEOW!');
  console.log('Someone made a request to /dog');  // This will show up in the Node console
});                                               // Can trigger from browser, Postman, etc.

// example of using route parameters to match reddit urls
app.get('/r/:subredditName', function(req, res) {
  console.log(req.params);
  res.send('WELCOME TO A SUBREDDIT!');
});

app.get('/r/:subredditName/comments/:id/:title/', function(req, res) {
  let subreddit = req.params.subredditName;
  let title = req.params.title;
  res.send('WELCOME TO THE COMMENTS PAGE FOR THE POST "' + title.toUpperCase() + '"' + ' IN THE ' +
    subreddit.toUpperCase() + ' SUBBREDDIT!');
});


// provides a response when for when someone attempts a get on a non-existent route
// must go last so it doesn't catch all the get requests
app.get('*', function(req, res) {
  res.send('YOU ARE A STAR!!!');
});


// START SERVER
// tell Express to listen for requests
app.listen(3000, function(){    // app.listen(port, hostname, backlog, callback)
  console.log('Server has started on port 3000!!!');
});
