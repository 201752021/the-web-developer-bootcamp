// RESTful Blog App

let express = require('express'),
  app = express(),
  expressSanitizer = require('express-sanitizer'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override');
const mongoose = require('mongoose');


// -------------------
//     APP CONFIG
// -------------------

mongoose.connect('mongodb://localhost:27017/restful_blog_app', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));


app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());  // must be after bodyParser to work
app.use(methodOverride('_method'));


// ---------------------
// MONGOOSE/MODEL CONFIG
// ---------------------

let blogSchema = new mongoose.Schema({
  title: String,
  image: String,
  body: String,
  created: {type: Date, default: Date.now},
});

let Blog = mongoose.model('Blog', blogSchema);


// -------------------
//   RESTFUL ROUTES
// -------------------

// INDEX ROUTE
app.get('/', (req, res) => {
  res.redirect('/blogs');
});

app.get('/blogs', (req, res) => {
  Blog.find({}, (err, blogs) => {
    if (err) {
      console.log('ERROR!');
    } else {
      res.render('index', {blogs: blogs});
    }
  });
});

// NEW ROUTE
app.get('/blogs/new', (req, res) => {
  res.render('new');
});

// CREATE ROUTE
app.post('/blogs', (req, res) => {
  req.body.blog.body = req.sanitize(req.body.blog.body);
  // create blog
  Blog.create(req.body.blog, (err) => {
    if (err) {
      // if error re-render form
      res.render('new');
    } else {
      // redirect to index
      res.redirect('/blogs');
    }
  });
});

// SHOW ROUTE
app.get('/blogs/:id', (req, res) => {
  Blog.findById(req.params.id, (err, foundBlog) => {
    if (err) {
      res.redirect('/blogs');
    } else {
      res.render('show', {blog: foundBlog});
    }
  });
});

// EDIT ROUTE
app.get('/blogs/:id/edit', (req, res) => {
  Blog.findById(req.params.id, (err, foundBlog) => {
    if (err) {
      res.redirect('/blogs');
    } else {
      res.render('edit', {blog: foundBlog});
    }
  });
});

// UPDATE ROUTE
// this should be a put request, but html forms don't support put requests --- only get and post
// --- put requests will default to get requests --- to solve this we have to use the
// method-override package
app.put('/blogs/:id', (req, res) => {
  req.body.blog.body = req.sanitize(req.body.blog.body);
  Blog.findByIdAndUpdate(req.params.id, req.body.blog, (err) => {
    if(err) {
      res.redirect('/blogs');
    } else {
      res.redirect('/blogs/' + req.params.id);
    }
  })
});

// DESTROY ROUTE
// this should be a put request, but html forms don't support delete requests --- only get and post
// --- put requests will default to get requests --- to solve this we have to use the
// method-override package
app.delete('/blogs/:id', (req, res) => {
  // destroy blog
  Blog.findByIdAndRemove(req.params.id, (err) => {
    if(err) {
      res.redirect('/blogs');
    } else {
      // redirect to index page
      res.redirect('/blogs');
    }
  });
});


// -------------------
//    START SERVER
// -------------------

app.listen(3000, () => {
  console.log('Server is running.');
});
