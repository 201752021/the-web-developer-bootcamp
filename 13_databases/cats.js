// Demo using Mongoose to interact with MongoDb

const mongoose = require('mongoose');
// connect mongoose to MongoDB
mongoose.connect('mongodb://localhost:27017/cat_app', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

// defining a cat with a schema --- a pattern for our data --- can add still add things, but it
// gives
// us a
// structure to our data
let catSchema = new mongoose.Schema({
  name: String,
  age: Number,
  temperament: String
});
// currently any of those pieces of info could be left blank, but you can add requirements that
// they have to be filled --- like for an account sign-up or something where you don't want
// users to just be able to leave things blank


let Cat = mongoose.model('Cat', catSchema);
// compiled a model with our catSchema and a name (the name should be the singular of the
// collection) and assigned it to the variable Cat --- now we'll do everything off of the Cat
// object --- the model has all the methods we need to use on it

// ***** ADDING NEW CATS TO THE DB *****

let george = new Cat({
  name: 'George',
  age: 7,
  temperament: 'grouchy'});
// adding the callback function to the save method allows us to make sure nothing went wrong and it
// actually saved
george.save(function(err, cat) {  // george is what we have in javascript, but cat is what's
  if(err) {                       // actually being saved and sent back from the db
    console.log("SOMETHING WENT WRONG!");
  } else {
    console.log('WE JUST SAVED A CAT TO THE DB:');
    console.log(cat);
  }
});
// pretty much everything in mongoose will have callback functions because it will take time ---
// callbacks allow code to execute after it is done ????? ~2:45, lecture 306

george = new Cat({
  name: 'Mrs. Norris',
  age: 7,
  temperament: 'Evil'});
// adding the callback function to the save method allows us to make sure nothing went wrong and it
// actually saved
george.save(function(err, cat) {  // george is what we have in javascript, but cat is what's
  if(err) {                       // actually being saved and sent back from the db
    console.log("SOMETHING WENT WRONG!");
    console.log(err);
  } else {
    console.log('WE JUST SAVED A CAT TO THE DB:');
    console.log(cat);
  }
});

// ***** CREATING CATS ***** (instead of making new ones and then saving them as two steps)

Cat.create({
  name: 'Snow White',
  age: 15,
  temperament: 'bland'
}, function (err, cat) {
  if(err) {                       // actually being saved and sent back from the db
    console.log(err);
  } else {
    console.log(cat);
  }
});

// ***** RETRIEVE ALL CATS FROM THE DB AND CONSOLE.LOG EACH ONE *****

Cat.find({}, function(err, cats) {
  if(err) {
    console.log('OH NO, ERROR!');
    console.log(err);
  } else {
    console.log('ALL THE CATS.......');
    console.log(cats);
  }
});
