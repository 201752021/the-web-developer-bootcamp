// Node Exercise 2
// AVERAGE GRADE

// define new function named 'average'
// it takes a single parameter: an array of test scores (all numbers)
// it should return the average score in the array, rounded to the nearest whole number

function average(grades) {
  let total = 0;
// loop I wrote
  // for(let i = 0; i < grades.length; i++) {
  //   total += grades[i];
  // }

// Colt used forEach loop in solution
  grades.forEach(function(grade) {
    total += grade;
  });
  let averageGrade = total/grades.length;
  let roundedAverage = Math.round(averageGrade);
  // console.log(roundedAverage);
  // he said to return, not print the average
  return roundedAverage;
}

// examples given to run in video lecture
// added 'console.log's in solution video to see if function worked
console.log('Average score for environmental science');
let scores = [90, 98, 89, 100, 100, 86, 94];
console.log(average(scores));  // should return 94

console.log('Average score for organic chemistry');
let scores2 = [40, 65, 77, 82, 80, 54, 73, 63, 95, 49];
console.log(average(scores2));  // should return 68

// run from command line using node
