// Node Exercise 1

// write a function named 'echo' that takes two arguments: a string and a number
// it should print out the string, number number of times

function echo(str, num) {
  for(let i = 0; i < num; i++) {
    console.log(str);
  }
}

// examples given to run in video lecture
echo('Echo!!!', 10)   // should print 'Echo!!!' 10 times
echo('TaterTots', 3)  // should print 'TaterTots' 3 times


// finally, run the contents of this file using node
