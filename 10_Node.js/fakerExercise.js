// NPM Exercise

// read faker docs and figure out how to print 10 random names and prices
let fakeProduct = require('faker');

console.log('=====================');
console.log(' WELCOME TO MY SHOP!');
console.log('=====================');

for(let i = 0; i < 10; i++) {
  let productName = fakeProduct.commerce.productName();
  let productPrice = fakeProduct.commerce.price();
  console.log(productName + ' - $' + productPrice);
}
