// ***** YelpCamp App *****

// -------------------
//     APP SETUP
// -------------------

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  Campground = require('./models/campground'),
  Comment = require('./models/comment'),
  mongoose = require('mongoose'),
  seedDB = require('./seeds');

mongoose.connect('mongodb://localhost:27017/yelp_camp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));
// adding __dirname makes it safer because if something gets messed up with the files __dirname
// will always be the directory this script lives in --- more conventional way than how we did
// it before


// -------------------
//    SEED DATABASE
// -------------------

seedDB();


// -------------------
//     GET ROUTES
// -------------------

// LANDING PAGE
app.get('/', (req, res) => {
  res.render('landing');
});

// INDEX ROUTE
app.get('/campgrounds', (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/index', {campgrounds: allCampgrounds});
    }
  });
});

// NEW ROUTE
app.get('/campgrounds/new', (req, res) => {
  res.render('campgrounds/new');
});

// NESTED NEW ROUTE --- shows new comments form
app.get('/campgrounds/:id/comments/new', (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('comments/new', {campground: campground});
    }
  });
});

// SHOW ROUTE
app.get('/campgrounds/:id', (req, res) => {
  Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/show', {campground: foundCampground});
    }
  });
});


// -------------------
//    POST ROUTES
// -------------------

// CREATE ROUTE
app.post('/campgrounds', (req, res) => {
  let name = req.body.name;
  let image = req.body.image;
  let desc = req.body.description;
  let newCampground = {name: name, image: image, description: desc};

  Campground.create(newCampground, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/campgrounds');
    }
  });
});

// NESTED CREATE ROUTE --- posts the new comment
app.post('/campgrounds/:id/comments', (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
      res.redirect('/campgrounds');
    } else {
      // create new comment
      Comment.create(req.body.comment, (err, comment) => {
        if (err) {
          console.log(err);
        } else {
          // connect new comment to campground
          campground.comments.push(comment);   // campground from line 104
          campground.save();
          // redirect campground to show page
          res. redirect('/campgrounds/' + campground._id);
        }
      });
    }
  });
});


// -------------------
//      SERVER
// -------------------

app.listen(3000, () => {
  console.log('The YelpCamp server has started.');
});
