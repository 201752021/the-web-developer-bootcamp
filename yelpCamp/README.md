# YelpCamp

--> NOTE: There are different versions so I can look back and see the steps and what was added and
 changed along the way. Each version also has the notes about features added in that version. The
  notes are deleted at the start of the next version so the code isn't too cluttered.

## Lecture 294: Initial Setup
**--> NOTE: start of v1**

* add Landing Page
* add Campgrounds Page that lists all campgrounds

Each Campground has:
* name
* image

## Lecture 296: Layout and Basic Styling

* create our header and footer partials
* add in Bootstrap

## Lecture 297: Creating New Campgrounds

* setup new campground POST route
* add in body-parser
* setup route to show form
* add basic unstyled form

## Lecture 299: Style the Campgrounds Page

* add a better header/title
* make campgrounds display in a grid

## Lecture 300: Style the Navbar and Form

* add a navbar to all templates
* style the new campgound form

## Lecture 308: Adding Mongoose
**--> NOTE: switches to v2**

* install and configure mongoose
* setup campground model
* use campground model inside our routes

## Lectures 310 & 311: Campground Show Page

* review the RESTful routes we've seen so far
* add description to our campground model
* show db.collection.drop()
* add a show route/template

## Lecture 329: Refactor Mongoose Code in app.js
**--> NOTE: switches to v3**

* create a models directory
* use module.exports
* require everything correctly

## Lecture 331: Seeding the Database

* add a seeds.js file
* run the seeds file every time the server starts

## Lecture 333: Adding the Comment Model

* make our errors go away
* display comments on the campground show page

## Lectures 335 & 336: Creating Comments
**--> NOTE: switches to v4**

* discuss nested routes
* add the comment new and create routes
* add the new comment form

## Lectures 337 & 339: Style the Show Page
**--> NOTE: switches to v5**

* add a sidebar to the show page
* display comments nicely

## Lecture 347: Auth - Add User Model
**--> NOTE: switches to v6**

* install all packages needed for auth
* define user model

## Lecture 348: Auth - Register

* configure passport
* add register routes
* add register template

## Lecture 349: Auth - Login

* add login routes
* add login template

## Lecture 350: Auth - Logout & Navbar

* add logout route
* prevent user from adding a comment if not signed in
* add links to navbar

## Lecture 351: Auth - Show/Hide Links

* show/hide auth links correctly

## Lecture 352: Refactoring the Routes
**--> NOTE: switches to v7**

* use express router to reorganize all routes

## Lecture 353: User Associations - Comments
**--> NOTE: switches to v8**

* associate users and co
mments
* save author's name to a comment automatically

## Lecture 354: User Associations - Campgrounds
**--> NOTES: switches to v9**

* prevent an unauthorized user from creating a campground
* save username and id to newly created campground

## Lecture 356: Campground Update and Edit
**--> NOTES: switches to v10**

* add method-override
* add edit route for campgrounds
* add a link to the edit page
* add update route

## Lecture 357: Deleting Campgrounds

* add delete route
* add delete button

## Lecture 358(note by Ian): Removing Associated Comments
(instructions are in the comments on the lecture)

* add pre-hook to campground model
* rewrite destroy route

## Lecture 359 & 360: Campground Authorization

* user can only edit his/her campgrounds
* user can only delete his/her campgrounds
* hide/show edit and delete buttons

## Lecture 361: Editing/Updating Comments

* add edit route for comments
* add edit button
* add update route

## Lecture 363: Deleting Comments

* add destroy route
* add delete button

## Lecture 364: Comment Authorization

* user can only edit his/her comments
* user can only delete his/her comments
* hide/show edit and delete buttons

## Lecture 365: Refactoring Middleware

* refactor middleware

## Lecture 366: Installing Flash Messages
**-->NOTE: switches to v11**

* install and configure connect-flash
* add bootstrap alerts to header

## Lecture 368: Adding Bootstrap to Flash Messages

* move flash code to header

## Lecture 369: Error Handling

* add helpful flash error messages

## Lecture 371 & 372: Landing Page Refactor
**-->NOTE: switches to v12**

* adding the background slider 

## Lecture 373: Dynamic Price Feature

* add price to campground model as a String datatype
* add price to the campground new and edit forms
* add price to the show page

## Lecture 384: Deploying YelpCamp - Basics
**-->NOTE: switches to v12deployed**

* https://cryptic-meadow-28195.herokuapp.com/ | https://git.heroku.com/cryptic-meadow-28195.git

## Environment Variables

* create environment variable
    * allows testing of new features, etc without messing up the production site
* set local env via command line
* set production env on Heroku
    * in Settings-->Config Vars
    