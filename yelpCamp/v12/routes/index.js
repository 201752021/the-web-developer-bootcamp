// *** INDEX ROUTES ***

let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  User = require('../models/user');

// ROOT
router.get('/', (req, res) => {
  res.render('landing');
});

// NEW REGISTER
router.get('/register', (req, res) => {
  res.render('register');
});

// CREATE REGISTER
router.post('/register', (req, res) => {
  let newUser = new User({username: req.body.username});
  User.register(newUser, req.body.password, (err) => {
    if (err) {
      // TODO: not showing err.message
      req.flash('error', err.message);
      console.log(err);
      return res.render('register');
    }
    passport.authenticate('local')(req, res, () => {
      req.flash('success', 'Welcome to Yelpcamp, ' + req.body.username);
      res.redirect('/campgrounds');
    });
  });
});

// NEW LOGIN
router.get('/login', (req, res) => {
  res.render('login');
});

// CREATE LOGIN
router.post('/login', function (req, res, next) {
  passport.authenticate(
    'local',
    {
      successRedirect: '/campgrounds',
      successFlash: 'Welcome back, ' + req.body.username + '!',
      failureRedirect: '/login',
      failureFlash: true,
    },
  )(req, res);
});

// LOGOUT
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success', 'You have logged out!');
  res.redirect('/campgrounds');
});

module.exports = router;
