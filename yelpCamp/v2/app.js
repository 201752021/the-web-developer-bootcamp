// ***** YelpCamp App *****


// -------------------
//     APP SETUP
// -------------------

// express is a web framework for Node.js (https://www.npmjs.com/package/express)
// body-parser is how we get data out of our forms (https://www.npmjs.com/package/body-parser)
// axios lets us make requests from the API (https://www.npmjs.com/package/axios)
// mongoose is to interact with MongoDB (https://mongoosejs.com/)
let express    = require('express'),
    app        = express(),                     // separating by commas and lining up equals is a
    bodyParser = require('body-parser');     // common stylistic choice, no difference functionally
// const axios    = require('axios'),
    mongoose   = require('mongoose');

mongoose.connect('mongodb://localhost:27017/yelp_camp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));


// -------------------
//    SCHEMA SETUP
// -------------------

let campgroundSchema = new mongoose.Schema({
  name: String,
  image: String,
  description: String
});

let Campground = mongoose.model('campground', campgroundSchema);

// Campground.create(
//   {
//     name : "Dead Horse Ranch State Park",
//     image : "https://www.outdoorproject.com/sites/default/files/styles/hero_image_desktop/public/features/_dsc7088.jpg",
//     description: 'This campground doesn\'t really contain dead horses.'
//   }, (err, campground) => {
//     if(err) {
//       console.log(err);
//     } else {
//       console.log('NEWLY CREATED CAMPGROUND');
//       console.log(campground);
//     }
//   });


// -------------------
//     GET ROUTES
// -------------------

app.get('/', (req, res) => {
  res.render('landing');
});

// INDEX ROUTE - show all campgrounds
app.get('/campgrounds', (req, res) => {
  // get all campgrounds from DB
  Campground.find({}, (err, allCampgrounds) => {
    if(err) {
      console.log(err);
    } else {
      res.render('index', {campgrounds: allCampgrounds});    // first variable,
      // 'campgrounds', is the same campgrounds variable on the campgrounds page, but now the second
      // variable, 'allCampgrounds', isn't from an array any more, it is defined above in the
      // callback function of .find
    }
  });
});

// NEW ROUTE - show form to create new campground
app.get('/campgrounds/new', (req, res) => {
  res.render('new');
});

// SHOW ROUTE - where we can click through and view more info on one specific campground
// --- putting this after other `/campgrounds/{whatever is here}` routes insures those routes will
// still work and this one won't become a catch all for anything that's not the index page
app.get('/campgrounds/:id', (req, res) => {
  // find the campground with provided ID --- the IDs are assigned by mongo
  Campground.findById(req.params.id, (err, foundCampground) => {
    if(err) {
      console.log(err);
    } else {
      // render show template with that campground
      res.render('show', {campground: foundCampground});
    }
  });
});


// -------------------
//    POST ROUTES
// -------------------

// CREATE ROUTE - add new campground to DB
app.post('/campgrounds', (req, res) => {
  let name = req.body.name;
  let image = req.body.image;
  let desc = req.body.description;
  let newCampground = {name: name, image: image, description: desc};
  // Create a new campground and save to DB
  Campground.create(newCampground, (err) => {
    if(err) {
      console.log(err);
    } else {
      res.redirect('/campgrounds');
    }
  });
});


// -------------------
//      SERVER
// -------------------

app.listen(3000, () => {
  console.log('The YelpCamp server has started.');
});
