// *** COMMENTS ROUTES ***

let express = require('express'),
  router = express.Router({mergeParams: true}),   // merges params from campground and
  Campground = require('../models/campground'),       // comments, so we can access the
  Comment = require('../models/comment');             // campground id in our comment routes

// NEW
router.get('/new', isLoggedIn, (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('comments/new', {campground: campground});
    }
  });
});

// CREATE
router.post('/', isLoggedIn, (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
      res.redirect('/campgrounds');
    } else {
      Comment.create(req.body.comment, (err, comment) => {
        if (err) {
          console.log(err);
        } else {
          // add username and id
          comment.author.id = req.user._id; // we know there is a user because we can't get to
                                            // this code without a user being logged in due to
                                            // the isLoggedIn middleware

                                            // the format is comment.author.id because that is
                                            // how it is setup in the comment model
          comment.author.username = req.user.username;
          // save comment
          comment.save();
          campground.comments.push(comment);
          campground.save();
          res.redirect('/campgrounds/' + campground._id);
        }
      });
    }
  });
});


// -------------------
//    MIDDLEWARE
// -------------------

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}

module.exports = router;
