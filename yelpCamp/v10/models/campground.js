// CAMPGROUND MODEL

let mongoose = require('mongoose');
const Comment = require('./comment');

let campgroundSchema = new mongoose.Schema({
  name: String,
  image: String,
  description: String,
  author: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    username: String
  },
  comments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Comment'
    }
  ]
});

campgroundSchema.pre('remove', async function() {     // this is called a pre-hook. it is being
  await Comment.remove({                              // added so the comments associated with
    _id: {                                            // a deleted campground will be removed from
      $in: this.comments                              // the database
    }
  });
});

module.exports = mongoose.model('Campground', campgroundSchema);
