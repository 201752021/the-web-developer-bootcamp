// ***** YelpCamp App *****

let express = require('express');
let app = express();
let bodyParser = require('body-parser');
const axios = require('axios');

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));

let campgrounds = [
  {
    name: 'Dead Horse Ranch State Park',
    image: 'https://www.outdoorproject.com/sites/default/files/styles/hero_image_desktop/public/features/_dsc7088.jpg?itok=1HJLQ1qm',
  },
  {
    name: 'Colossal Cave Mountain Park',
    image: 'https://colossalcave.com/wp-content/uploads/2016/09/CCMP-Camping500x333.jpg',
  },
  {name: 'Arcadia Campground', image: 'https://www.fs.usda.gov/Internet/FSE_MEDIA/fseprd746265.jpg'},
  {
    name: 'Dead Horse Ranch State Park',
    image: 'https://www.outdoorproject.com/sites/default/files/styles/hero_image_desktop/public/features/_dsc7088.jpg?itok=1HJLQ1qm',
  },
  {
    name: 'Colossal Cave Mountain Park',
    image: 'https://colossalcave.com/wp-content/uploads/2016/09/CCMP-Camping500x333.jpg',
  },
  {name: 'Arcadia Campground', image: 'https://www.fs.usda.gov/Internet/FSE_MEDIA/fseprd746265.jpg'},
  {
    name: 'Dead Horse Ranch State Park',
    image: 'https://www.outdoorproject.com/sites/default/files/styles/hero_image_desktop/public/features/_dsc7088.jpg?itok=1HJLQ1qm',
  },
  {
    name: 'Colossal Cave Mountain Park',
    image: 'https://colossalcave.com/wp-content/uploads/2016/09/CCMP-Camping500x333.jpg',
  },
  {name: 'Arcadia Campground', image: 'https://www.fs.usda.gov/Internet/FSE_MEDIA/fseprd746265.jpg'},
  {
    name: 'Dead Horse Ranch State Park',
    image: 'https://www.outdoorproject.com/sites/default/files/styles/hero_image_desktop/public/features/_dsc7088.jpg?itok=1HJLQ1qm',
  },
  {
    name: 'Colossal Cave Mountain Park',
    image: 'https://colossalcave.com/wp-content/uploads/2016/09/CCMP-Camping500x333.jpg',
  },
  {name: 'Arcadia Campground', image: 'https://www.fs.usda.gov/Internet/FSE_MEDIA/fseprd746265.jpg'}
];

// -------------------
//     GET ROUTES
// -------------------

// makes landing page (homepage)
app.get('/', function (req, res) {
  res.render('landing');
});

// displays campgrounds array
app.get('/campgrounds', function (req, res) {
  res.render('campgrounds', {campgrounds: campgrounds});
});

// shows the form to add a new campground
app.get('/campgrounds/new', (req, res) => {
  res.render('new');
});

// -------------------
//    POST ROUTES
// -------------------

// handles logic of making a new campground, sends us back to '/campgrounds' where the
// '/campgrounds' get route will show the campgrounds array with the campground added
app.post('/campgrounds', (req, res) => {
  // get data from form and add to campgrounds array
  let name = req.body.name;
  let image = req.body.image;
  let newCampground = {name: name, image: image};
  campgrounds.push(newCampground);
  // redirect back to campgrounds page
  res.redirect('/campgrounds');
});
// naming this the same as the get route is a REST convention (learn about REST in a later lecture)

app.listen(3000, function () {
  console.log('The YelpCamp server has started.');
});
