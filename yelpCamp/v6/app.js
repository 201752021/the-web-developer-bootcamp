// ***** YelpCamp App *****

// -------------------
//     APP SETUP
// -------------------

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  Campground = require('./models/campground'),
  Comment = require('./models/comment'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  LocalStrategy = require('passport-local'),
  seedDB = require('./seeds'),
  User = require('./models/user');

mongoose.connect('mongodb://localhost:27017/yelp_camp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));


// -------------------
//    SEED DATABASE
// -------------------

seedDB();


// -------------------
//   PASSPORT CONFIG
// -------------------
app.use(require('express-session')({
  secret: 'I miss my sweet Tali Tigershark <3',
  resave: false,
  saveUninitialized: false,
}));
app.use(passport.initialize({}));
app.use(passport.session({}));
passport.use(new LocalStrategy({}, User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser((User.deserializeUser()));
app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  next();
});


// -------------------
//      ROUTES
// -------------------

// LANDING PAGE
app.get('/', (req, res) => {
  res.render('landing');
});

// *** CAMPGROUND ROUTES ***

// INDEX ROUTE
app.get('/campgrounds', (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/index', {campgrounds: allCampgrounds,});
    }
  });
});

// NEW ROUTE
app.get('/campgrounds/new', (req, res) => {
  res.render('campgrounds/new');
});

// CREATE ROUTE
app.post('/campgrounds', (req, res) => {
  let name = req.body.name;
  let image = req.body.image;
  let desc = req.body.description;
  let newCampground = {name: name, image: image, description: desc};

  Campground.create(newCampground, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/campgrounds');
    }
  });
});

// SHOW ROUTE
app.get('/campgrounds/:id', (req, res) => {
  Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/show', {campground: foundCampground});
    }
  });
});

// *** COMMENT ROUTES ***

// NEW ROUTE
app.get('/campgrounds/:id/comments/new', isLoggedIn, (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('comments/new', {campground: campground});
    }
  });
});

// CREATE ROUTE
app.post('/campgrounds/:id/comments', isLoggedIn, (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
      res.redirect('/campgrounds');
    } else {
      Comment.create(req.body.comment, (err, comment) => {
        if (err) {
          console.log(err);
        } else {
          campground.comments.push(comment);
          campground.save();
          res.redirect('/campgrounds/' + campground._id);
        }
      });
    }
  });
});

// *** AUTH ROUTES ***

// NEW ROUTE
app.get('/register', (req, res) => {
  res.render('register');
});

// CREATE ROUTE
app.post('/register', (req, res) => {
  let newUser = new User({username: req.body.username});
  User.register(newUser, req.body.password, (err) => {
    if (err) {
      console.log(err);
      return res.render('register');
    }
    passport.authenticate('local')(req, res, () => {
      res.redirect('/campgrounds');
    });
  });
});

// *** LOGIN ROUTES ***

// NEW ROUTE
app.get('/login', (req, res) => {
  res.render('login');
});

// CREATE ROUTE
app.post(
  '/login',
  passport.authenticate(
    'local',
    {
      successRedirect: '/campgrounds',
      failureRedirect: '/login',
    },
  )
);

// *** LOGOUT ROUTE ***

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/campgrounds');
});


// -------------------
//    MIDDLEWARE
// -------------------

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}


// -------------------
//      SERVER
// -------------------

app.listen(3000, () => {
  console.log('The YelpCamp server has started.');
});
