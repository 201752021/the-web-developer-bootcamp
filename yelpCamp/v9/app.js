// ***** YelpCamp App *****

// -------------------
//  GENERAL REQUIRES
// -------------------

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  LocalStrategy = require('passport-local'),
  seedDB = require('./seeds'),
  User = require('./models/user');


// -------------------
//   ROUTE REQUIRES
// -------------------

let campgroundRoutes = require('./routes/campgrounds'),
  commentsRoutes = require('./routes/comments'),
  indexRoutes = require('./routes/index');


// -------------------
//   GENERAL CONFIG
// -------------------

mongoose.connect('mongodb://localhost:27017/yelp_camp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));


// -------------------
//    SEED DATABASE
// -------------------

// seedDB();


// -------------------
//   PASSPORT CONFIG
// -------------------

app.use(require('express-session')({
  secret: 'I miss my sweet Tali Tigershark <3',
  resave: false,
  saveUninitialized: false,
}));
app.use(passport.initialize({}));
app.use(passport.session({}));
passport.use(new LocalStrategy({}, User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser((User.deserializeUser()));
app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  next();
});


// -------------------
//    ROUTES CONFIG
// -------------------

app.use(indexRoutes);
app.use('/campgrounds', campgroundRoutes);
app.use('/campgrounds/:id/comments', commentsRoutes);


// -------------------
//      SERVER
// -------------------

app.listen(3000, () => {
  console.log('The YelpCamp server has started.');
});
