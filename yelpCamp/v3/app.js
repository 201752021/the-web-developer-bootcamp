// ***** YelpCamp App *****

// -------------------
//     APP SETUP
// -------------------

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  Campground = require('./models/campground'),
  Comment = require('./models/comment'),
  mongoose = require('mongoose'),
  seedDB = require('./seeds');

mongoose.connect('mongodb://localhost:27017/yelp_camp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: true}));


// -------------------
//    SEED DATABASE
// -------------------

seedDB();


// -------------------
//     GET ROUTES
// -------------------

app.get('/', (req, res) => {
  res.render('landing');
});

// INDEX ROUTE
app.get('/campgrounds', (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log(err);
    } else {
      res.render('index', {campgrounds: allCampgrounds});
    }
  });
});

// NEW ROUTE
app.get('/campgrounds/new', (req, res) => {
  res.render('new');
});

// SHOW ROUTE
app.get('/campgrounds/:id', (req, res) => {
  // find the campground with provided ID and populate the comments array
  Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
    if (err) {
      console.log(err);
    } else {
      console.log(foundCampground); // lets us see the populated array
      res.render('show', {campground: foundCampground});
    }
  });
});


// -------------------
//    POST ROUTES
// -------------------

// CREATE ROUTE
app.post('/campgrounds', (req, res) => {
  let name = req.body.name;
  let image = req.body.image;
  let desc = req.body.description;
  let newCampground = {name: name, image: image, description: desc};

  Campground.create(newCampground, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/campgrounds');
    }
  });
});


// -------------------
//      SERVER
// -------------------

app.listen(3000, () => {
  console.log('The YelpCamp server has started.');
});
