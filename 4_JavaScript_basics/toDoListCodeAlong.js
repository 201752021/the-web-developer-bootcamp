let todos = [];

function listToDos() {
  //print the array
  console.log('*******************');
  todos.forEach(function (todo, index) {
    console.log(index + ': ' + todo);
  });
  console.log('*******************');
}

function addToDo() {
  //ask for new list item
  let newTodo = prompt('Enter new ToDo')
  //add to array
  todos.push(newTodo)
  console.log('Added ToDo')
}

function deleteToDo() {
  //ask for index to be deleted
  let index = Number(prompt('Enter the index of the ToDo to delete'));
  //delete that index
  todos.splice(index, 1);
  console.log('Deleted ToDo');
}

window.setTimeout(function(){
  let input = prompt('What would you like to do?')

  while(input !== 'quit'){
    //handle input
    if (input === 'list') {
      listToDos();
    } else if(input === 'new') {
      addToDo();
    } else if(input === 'delete'){
      deleteToDo();
    }
    //ask again for new input
    input = prompt('What would you like to do?')
  }
  console.log("OK, YOU QUIT THE APP");
},500);
