// create secretNumber
let secretNumber = 4;

// ask user for guess
let guess = Number(prompt("Guess a number"));
  // some people think it is unnecessary to do this on one line and prefer to do it on multiple
// lines
  // let stringGuess = prompt("Guess a number");
  // let guess = Number(stringGuess);

// check if guess is right
if(guess === secretNumber){
  alert("YOU GOT IT RIGHT!");
}

// otherwise, determine if guess is higher or lower
else if(guess > secretNumber){
  alert("Too high. Guess again!");
  location.reload();
}
else {
  alert("Too low. Guess again!");
  location.reload()
}
