// print all numbers between -10 and 19
console.log("PRINT ALL NUMBERS BETWEEN -10 AND 19");
let count1 = -10;
while(count1 <= 19){
  console.log(count1);
  count1++;
}


// print all even numbers between 10 and 40
console.log("PRINT ALL EVEN NUMBERS BETWEEN 10 AND 40");
let count2 = 10;
while(count2 <= 40){
  console.log(count2);
  count2+=2;
}
// I wrote it this way, but the other was is more efficient because the loop doesn't run through
// every number

// let count2 = 10;
// while(count2 <= 40){
//   if(count2 % 2 === 0){
//     console.log(count2);
//   }
//   count2++;
// }


// print all odd numbers between 300 and 333
console.log("PRINT ALL ODD NUMBERS BETWEEN 300 AND 333");
let count3 = 301;
while(count3 <= 333){
  console.log(count3);
  count3+=2;
}
// I wrote it this way (as did Colt in solutions video), but the other seems more efficient
// because the loop doesn't run through every number

// let count3 = 300;
// while(count3 <= 333){
//   if(count3 % 2 !== 0){
//     console.log(count3);
//   }
//   count3++;
// }


// print all numbers divisible by 5 AND 3 between 5 and 50
console.log("PRINT ALL NUMBERS DIVISIBLE BY 5 AND 3, BETWEEN 5 AND 50");
let count4 = 5;
while(count4 <= 50){
  if((count4 % 5 === 0) && (count4 % 3 === 0)){   // don't need parenthesis around each part of the
    console.log(count4);                          // && --- just easier to read
  }
  count4++;
}
